const api = './api/';

function exibeForm(opt){
    if(opt === true){
        $('#form').show();
        $('#btn_novo').hide();
    } else {
        $('#form').hide();
        $('#btn_novo').show();
    }
}

function departamentos() {
    $.get(api + 'departamento/lista.php', function(data) {
        const select = $('#departamento_id');
        $.each(data, function (i, d) {
            $('<option>').val(d.id).text(d.nome).appendTo(select);
        });
    });
}

function contatos() {
    const get = $.get(api + 'contato/lista.php');
    get.done(function( data ) {
        const table = $('#table-contatos');
        $("#table-contatos tbody tr").remove();

        $.each(data, function (i, d) {
            const newRow = $("<tr>");
            let cols = "";

            const nascimento = d.nascimento !== null ? d.nascimento : '';
            const sexo = d.sexo !== null ? d.sexo : '';

            cols += '<td>' + d.id + '</td>';
            cols += '<td>' + d.nome + '</td>';
            cols += '<td>' + d.sobrenome + '</td>';
            cols += '<td>' + nascimento  + '</td>';
            cols += '<td>' + sexo + '</td>';
            cols += '<td>';
            cols += '<button class="btn btn-primary btn-sm" type="button" onclick="editar(' + d.id + ')">Editar</button>';
            cols += '</td>';
            cols += '<td>';
            cols += '<button class="btn btn-danger btn-sm" onclick="excluir(' + d.id + ')" type="button">Remover</button>';
            cols += '</td>';

            newRow.append(cols);
            table.append(newRow);
        });
    });
    get.fail(function( error) {
        console.warn(error.responseText);
        alert('Ops, ocorreu um erro!');
    });
}

function editar(id){
    limpaFones();
    $.get(api + 'contato/detail.php?id=' + id, function(data) {
        $('#id').val(data.id);
        $('#nome').val(data.nome);
        $('#sobrenome').val(data.sobrenome);
        $('#nascimento').val(data.nascimento);
        $('#sexo').val(data.sexo);
        $('#departamento_id').val(data.departamento_id);
        //
        $.each(data.phones, function (i, d) {
            addFone(d.numero);
        });
        //
        exibeForm(true);
    });
}

function excluir(id){
    const post = $.post(api + 'contato/delete.php?id=' + id, {});
    post.done(function( data ) {
        contatos();
        alert("Excluído com sucesso!");
    }); 
}

function limpaForm()
{
    $('#id').val('');
    $('#nome').val('');
    $('#sobrenome').val('');
    $('#nascimento').val('');
    $('#sexo').val('');
    $('#departamento_id').val('');
    //
    limpaFones();
}

function addFone(numero)
{
    numero = numero == undefined ? "" : numero;

    const index = $('input[name="fones[]"]').length;
    
    let append = '<div class="form-check form-check-inline fone" id="fone-' + index +'" >';
    append += '<input class="form-control" type="text" name="fones[]" value="' + numero + '">';
    append += '<label><a href="#" onclick="deleteFone(' + index + ')" >X</a></label>';
    append += '</div>';
    
    $('#fones').append(append);
}

function limpaFones()
{
    $('.fone').remove();
}

function deleteFone(index)
{
    event.preventDefault();
    $('#fone-' + index).remove();
}


$('#btn_novo').click(function(){
    exibeForm(true);
});

$('#btn_fecha_form').click(function(){
    exibeForm(false);
});

$('#btn_salvar').click(function(){
    event.preventDefault();

    const id = $('#id').val();
    const url = id > 0 ? 'contato/edit.php?id=' + id : 'contato/insert.php';
    const data = {
        nome: $('#nome').val(),
        sobrenome: $('#sobrenome').val(),
        nascimento: $('#nascimento').val() || null,
        sexo: $('#sexo').val() || null,
        departamento_id: $('#departamento_id').val() || null,
        fones: $('input[name="fones[]"]').map(function(){return $(this).val();}).get()
    }

    if(!data.nome || !data.sobrenome) {
        alert("Preencha os camppos obrigatórios!");
        return false;
    }

    const post = $.post(api + url, data);
    post.done(function( data ) {
        exibeForm(false);
        limpaForm();
        contatos();
        alert("Realizado com sucesso!");
    });        
})

$('#btn_addFone').click(function(){
    event.preventDefault();
    addFone();
})   



// start
exibeForm(false);
contatos();
departamentos();
    
