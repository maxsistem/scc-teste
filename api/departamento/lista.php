<?php
header('Content-Type: application/json');

include '../crud/crud.php';

function all()
{
    try {
        $crud = new Crud();
        $sql = "Select * from departamentos";
        $data = $crud->getAll($sql);
        return json_encode($data);
    } catch (\Throwable $th) {
        return json_encode(['error' => $th->getMessage()]);
    }
}

echo all();