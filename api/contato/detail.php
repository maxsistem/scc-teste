<?php
header('Content-Type: application/json');

include '../crud/crud.php';


function all()
{
    try {
        $id = isset($_GET['id']) ? $_GET['id'] : null;

        if($id){
            $sql = "select contatos.id as id, contatos.nome, sobrenome, nascimento, sexo, departamento_id, departamentos.nome as departamentos_nome";
            $sql .= " from contatos";
            $sql .= " left join departamentos on contatos.departamento_id = departamentos.id";            
            $sql .= " where contatos.id = " . $id;
            
            $crud = new Crud();
            $data = $crud->getAll($sql);

            $res = [];
            if(count($data) > 0) {
                $res = $data[0];
                $res['phones'] = phones($data[0]['id']);
            }           

            return json_encode($res);
            
        } else {
            return json_encode(['msg' => 'Informe o id do contato']);
        }
        
    } catch (\Throwable $th) {
        return json_encode(['error' => $th->getMessage()]);
    }
}


function phones($contato_id)
{
    try {
        $sql = "select id, numero from telefones where contato_id = " . $contato_id;
        $crud = new Crud();
        return $crud->getAll($sql);

    } catch (\Throwable $th) {
        return $th->getMessage();
    }
    
}


echo all();