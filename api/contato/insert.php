<?php

include '../crud/crud.php';


function create()
{
    try {
        $crud = new Crud();

        $dataForm = $_REQUEST;
        
        $phones = [];
        if(isset($dataForm['fones'])) {
            $phones = $dataForm['fones'];
            unset($dataForm['fones']);
        }

        $data = $crud->prepareFields($dataForm);

        $id = $crud->create('contatos', $data);

        if($id > 0 ){
            createPhones($id, $phones);
        }

        return json_encode(['id' => $id ]);

    } catch (\Throwable $th) {
        return json_encode(['error' => $th->getMessage()]);
    }
    
}


function createPhones($contato_id, $phones)
{
    try {
        if(count($phones) > 0) {
            $crud = new Crud();
            foreach($phones as $phone) {
                $data = array(
                    'contato_id' => $contato_id,
                    'numero' => $phone
                );
                $data = $crud->prepareFields($data);
                $res = $crud->create('telefones', $data);

            }
        }        
    } catch (\Throwable $th) {
        return json_encode(['error' => $th->getMessage()]);
    }
}


echo create();