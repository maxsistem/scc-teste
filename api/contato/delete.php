<?php

header('Content-Type: application/json');

include '../crud/crud.php';


function delete() 
{
    try {
        $id = isset($_GET['id']) ? $_GET['id'] : null;

        if($id){
            $crud = new Crud();
            $res = $crud->delete('contatos', $id);
            
            return json_encode(['result' => $res > 0 ? true : false]);
 
        } else {
            return json_encode(['msg' => 'Informe o id do contato']);
        }   


    } catch (\Throwable $th) {
        return json_encode(['error' => $th->getMessage()]);
    }  

}

echo delete();