<?php

include '../crud/crud.php';


function edit()
{
    try {
        $id = isset($_GET['id']) ? $_GET['id'] : null;

        if($id){
            $crud = new Crud();

            $dataForm = $_REQUEST;
            
            $phones = [];
            if(isset($dataForm['fones'])) {
                $phones = $dataForm['fones'];
                unset($dataForm['fones']);
            }

            $data = $crud->prepareFields($dataForm);

            $res = $crud->edit('contatos', $data, $id);

            deletePhones($id);
               
            createPhones($id, $phones);

            return json_encode(['result' => $res > 0 ? true : false ]);
        } else {
            return json_encode(['msg' => 'Informe o id do contato']);
        }

    } catch (\Throwable $th) {
        return json_encode(['error' => $th->getMessage()]);
    }
    
}


function deletePhones($contato_id)
{
    try {
        $crud = new Crud();
        return $crud->delete('telefones', $contato_id, $field = 'contato_id');
    } catch (\Throwable $th) {
        return json_encode(['error' => $th->getMessage()]);
    }
}

function createPhones($contato_id, $phones)
{
    try {
        if(count($phones) > 0) {
            $crud = new Crud();
            foreach($phones as $phone) {
                $data = array(
                    'contato_id' => $contato_id,
                    'numero' => $phone
                );
                $data = $crud->prepareFields($data);
                $res = $crud->create('telefones', $data);

            }
        }        
    } catch (\Throwable $th) {
        return json_encode(['error' => $th->getMessage()]);
    }
}


echo edit();