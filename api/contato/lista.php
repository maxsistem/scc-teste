<?php
header('Content-Type: application/json');

include '../crud/crud.php';

function all()
{
    try {
        $crud = new Crud();
        $sql = "select id, nome, sobrenome, date_format(nascimento,'%d/%m/%Y') as nascimento, sexo from contatos order by id desc";
        $data = $crud->getAll($sql, false);
        return json_encode($data);
    } catch (\Throwable $th) {
        return json_encode(['error' => $th->getMessage()]);
    }
}

echo all();