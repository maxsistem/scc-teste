<?php

class Database
{
    private $host = "localhost";
    private $db = "scc-teste";
    private $user = "root";
    private $password = "1234";

    public function connection()
    {
        try {
            $opt = array(
                PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES UTF8'
            );
            $cnn = new PDO("mysql:host=" . $this->host . ";dbname=" . $this->db, $this->user, $this->password, $opt);
            $cnn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            return $cnn;
        } catch (PDOException $e) {
            echo "Error connection: " . $e->getMessage();
            exit;
        }
    }

}