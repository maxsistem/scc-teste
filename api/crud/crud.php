<?php
include 'database.php';

class Crud
{
    private $conn;

    public function __construct()
    {
        $database = new Database();
        $this->conn = $database->connection();
    }
    
    public function getAll($sql, $json = true)
    {
        try {
            $st = $this->conn->prepare($sql);
            $st->execute();
            $data = $st->fetchAll();
            $total_column = $st->columnCount();

            for ($counter = 0; $counter < $total_column; $counter ++) {
                $meta = $st->getColumnMeta($counter);
                $columns[] = $meta['name'];
            }

            $result = [];
            foreach($data as $row) {
                foreach($columns as $col) {
                    $item[$col] = $row[$col];
                }
                $result[] = $item;
            }

            if($json) {
                header('Content-Type: application/json; charset=utf-8');
            }
            return $result;
        } catch (\Throwable $th) {
            return $th->getMessage();
        }        
    }
    
    public function find($fields = '*', $table, $id, $json = true)
    {
        try {
            $sql = 'select ' . $fields . ' from ' . $table . ' where id =' . $id;
            $data = $this->getAll($sql, $json);
            
            return count($data) === 1 ? $data[0] : null;

        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }

    public function create($table, $data) 
    {
        try 
        {
            $keys = array_keys($data);
            $fields = implode(', ', $keys) ;
            $values = ":" . implode(', :', $keys);
            
            $sql = "insert into " . $table . " (" . $fields . ") values (" . $values . ")";
            $st = $this->conn->prepare($sql);
            $st->execute($data);

            return $this->conn->lastInsertId();

        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    
    public function edit($table, $data, $id) 
    {
        try 
        {
            $keys = array_keys($data);
            $sql = "update " . $table . " set ";
            $count = count($keys);
            $i=0;
            
            while ($i < $count) {
                $sql .= $keys[$i] . " = :" . $keys[$i] .  ($i < $count-1 ? ", " : "");    
                $i++;
            }

            $sql .= " where id = :id";

            $st = $this->conn->prepare($sql);
            $st->execute($data);

            return $st->rowCount();

        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }

    public function prepareFields($data)
    {
        $arr = [];
        $keys = array_keys($data);
        $i = 0;
        foreach($data as $item){
            if (isset($item) && $item) {
                $arr[$keys[$i]] = $item;                
            }
            $i++;
        }

        return $arr;
    }

    public function delete($table, $id, $field = 'id')
    {
        try {
            $sql = "delete from " . $table . " where " . $field . " = :id";
            $st = $this->conn->prepare($sql);
            $st->bindParam(':id', $id); 
            $st->execute();     
            return $st->rowCount();

        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
}
